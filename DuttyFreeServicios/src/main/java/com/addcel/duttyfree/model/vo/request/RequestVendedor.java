package com.addcel.duttyfree.model.vo.request;

import java.io.Serializable;

public class RequestVendedor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id_supervisor;
	private String id_user;
	private String id_tienda;

	public String getId_supervisor() {
		return id_supervisor;
	}

	public void setId_supervisor(String id_supervisor) {
		this.id_supervisor = id_supervisor;
	}

	public String getId_user() {
		return id_user;
	}

	public void setId_user(String id_user) {
		this.id_user = id_user;
	}

	public String getId_tienda() {
		return id_tienda;
	}

	public void setId_tienda(String id_tienda) {
		this.id_tienda = id_tienda;
	}
}