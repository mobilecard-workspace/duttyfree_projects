package com.addcel.duttyfree.model.vo;

import java.util.HashMap;

public class ComisionVO {
	Double comision;
	Double comisionPorcentaje;
	Double minComPorcentaje;
	Double minCom;
	Double tipoCambio;
	String afiliacion;
	HashMap<String, String> afiliaciones;
	
	public Double getComision() {
		return comision;
	}
	public void setComision(Double comision) {
		this.comision = comision;
	}
	public Double getComisionPorcentaje() {
		return comisionPorcentaje;
	}
	public void setComisionPorcentaje(Double comisionPorcentaje) {
		this.comisionPorcentaje = comisionPorcentaje;
	}
	public Double getMinComPorcentaje() {
		return minComPorcentaje;
	}
	public void setMinComPorcentaje(Double minComPorcentaje) {
		this.minComPorcentaje = minComPorcentaje;
	}
	public Double getMinCom() {
		return minCom;
	}
	public void setMinCom(Double minCom) {
		this.minCom = minCom;
	}
	public Double getTipoCambio() {
		return tipoCambio;
	}
	public void setTipoCambio(Double tipoCambio) {
		this.tipoCambio = tipoCambio;
	}
	public String getAfiliacion() {
		return afiliacion;
	}
	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
		if(afiliacion != null){
			this.afiliaciones = new HashMap<String, String>();
			String [] afl = this.afiliacion.split(",");
			String [] aflDetalle = null;
			for(String cad: afl){
				aflDetalle = cad.split(":");
				this.afiliaciones.put(aflDetalle[0], aflDetalle[1]);
			}
		}
	}
	public HashMap<String, String> getAfiliaciones() {
		return afiliaciones;
	}
	public void setAfiliaciones(HashMap<String, String> afiliaciones) {
		this.afiliaciones = afiliaciones;
	}
	
}
