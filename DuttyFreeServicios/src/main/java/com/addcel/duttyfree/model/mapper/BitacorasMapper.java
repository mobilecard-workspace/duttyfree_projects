package com.addcel.duttyfree.model.mapper;

import org.apache.ibatis.annotations.Param;

import com.addcel.duttyfree.model.vo.DuttyFreeDetalleVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	int addBitacoraProsa(TBitacoraProsaVO b);
	int addBitacora(TBitacoraVO b);
	int updateBitacoraProsa(TBitacoraProsaVO b);
	int updateBitacora(TBitacoraVO b);
	int addDfDetalle(DuttyFreeDetalleVO dfd);
	int updateDfDetalle(@Param(value="idBitacora")long idBitacora,@Param(value="status")int status);
}
