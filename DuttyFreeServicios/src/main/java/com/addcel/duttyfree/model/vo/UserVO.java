package com.addcel.duttyfree.model.vo;

import java.util.Date;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UserVO {
	private int idUser;
	private String nombres;
	private String apellidoP;
	private String apellidoM;
	private String email;
	private String telefono;
	private int idUserEmpresa;
	private int idTienda;
	private String password;
	private Date lastLogin;
	private Date fechaAlta;
	private int idSupervisor;
	private String login;
	private int loginCount;
	private int idStatus;
	private	int idRole;
	
	public UserVO() {		
	}

	public UserVO(int idUser, String nombres, String apellidoP,
			String apellidoM, String email, String telefono,
			int idUserEmpresa, int idTienda, String password,
			Date lastLogin, Date fechaAlta, int idSupervisor, String login,
			int loginCount, int idStatus, int idRole) {
		super();
		this.idUser = idUser;
		this.nombres = nombres;
		this.apellidoP = apellidoP;
		this.apellidoM = apellidoM;
		this.email = email;
		this.telefono = telefono;
		this.idUserEmpresa = idUserEmpresa;
		this.idTienda = idTienda;
		this.password = password;
		this.lastLogin = lastLogin;
		this.fechaAlta = fechaAlta;
		this.idSupervisor = idSupervisor;
		this.login = login;
		this.loginCount = loginCount;
		this.idStatus = idStatus;
		this.idRole = idRole;
	}
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP) {
		this.apellidoP = apellidoP;
	}
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM) {
		this.apellidoM = apellidoM;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public int getIdUserEmpresa() {
		return idUserEmpresa;
	}
	public void setIdUserEmpresa(int idUserEmpresa) {
		this.idUserEmpresa = idUserEmpresa;
	}
	public int getIdTienda() {
		return idTienda;
	}
	public void setIdTienda(int idTienda) {
		this.idTienda = idTienda;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Date getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	public int getIdSupervisor() {
		return idSupervisor;
	}
	public void setIdSupervisor(int idSupervisor) {
		this.idSupervisor = idSupervisor;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public int getLoginCount() {
		return loginCount;
	}
	public void setLoginCount(int loginCount) {
		this.loginCount = loginCount;
	}
	public int getIdStatus() {
		return idStatus;
	}
	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}
	public int getIdRole() {
		return idRole;
	}
	public void setIdRole(int idRole) {
		this.idRole = idRole;
	}
}
