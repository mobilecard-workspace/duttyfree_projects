package com.addcel.duttyfree.model.vo.pagos;

public class ProcomVO {
	// Atributos obligatorios
	private String total;
	private String currency;
	private String address;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String digest;
	private String urlMoneda;
	private String urlBack;
	
	private int idError;
	private String mensajeError;

	// Atributos Opcionales	
	private String email;
	
	public ProcomVO(int idError, String mensajeError) {
		this.idError = idError;
		this.mensajeError = mensajeError;
	}

	public ProcomVO(String total, String currency, String address,
			String orderId, String merchant, String store, String term,
			String digest, String urlBack, String usuario, String idTramite,
			String email, String urlMoneda) {
		super();
		this.total = total;
		this.currency = currency;
		this.address = address;
		this.orderId = orderId;
		this.merchant = merchant;
		this.store = store;
		this.term = term;
		this.digest = digest;
		this.urlBack = urlBack;	
		this.email=email;
		this.urlMoneda = urlMoneda;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getUrlBack() {
		return urlBack;
	}

	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdError() {
		return idError;
	}

	public void setIdError(int idError) {
		this.idError = idError;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public String getUrlMoneda() {
		return urlMoneda;
	}

	public void setUrlMoneda(String urlMoneda) {
		this.urlMoneda = urlMoneda;
	}
}
