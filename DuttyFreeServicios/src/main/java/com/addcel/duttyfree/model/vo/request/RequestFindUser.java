package com.addcel.duttyfree.model.vo.request;

import java.io.Serializable;

public class RequestFindUser implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nombres;
	private String paterno;
	private String materno;
	private String login;

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getPaterno() {
		return paterno;
	}

	public void setPaterno(String paterno) {
		this.paterno = paterno;
	}

	public String getMaterno() {
		return materno;
	}

	public void setMaterno(String materno) {
		this.materno = materno;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}
}