/**
 * 
 */
package com.addcel.duttyfree.model.vo.pagos;

import java.io.Serializable;

/**
 * @author ELopez
 *
 */
public class TBitacoraProsaVO implements Serializable{	
	private static final long serialVersionUID = -588506397265536970L;
	private long idBitacoraProsa;
	private long idBitacora;
	private long idUsuario;
	private String tarjeta;
	private String transaccion;
	private String autorizacion;
	private String fecha;
	private String bit_hora;
	private String concepto;
	private double cargo;
	private double comision;
	private String cx;
	private String cy;
	
	public TBitacoraProsaVO(long idBitacora,
			long idUsuario, String concepto, double cargo, double comision, String cx, String cy) {
		super();
		this.idBitacora = idBitacora;
		this.idUsuario = idUsuario;
		this.concepto = concepto;
		this.cargo = cargo;
		this.comision = comision;
		this.cx = cx;
		this.cy = cy;
	}
	public TBitacoraProsaVO() {
		
	}
	public long getIdBitacoraProsa() {
		return idBitacoraProsa;
	}
	public void setIdBitacoraProsa(long idBitacoraProsa) {
		this.idBitacoraProsa = idBitacoraProsa;
	}
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getTransaccion() {
		return transaccion;
	}
	public void setTransaccion(String transaccion) {
		this.transaccion = transaccion;
	}
	public String getAutorizacion() {
		return autorizacion;
	}
	public void setAutorizacion(String autorizacion) {
		this.autorizacion = autorizacion;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getBit_hora() {
		return bit_hora;
	}
	public void setBit_hora(String bit_hora) {
		this.bit_hora = bit_hora;
	}
	public String getConcepto() {
		return concepto;
	}
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	public double getCargo() {
		return cargo;
	}
	public void setCargo(double cargo) {
		this.cargo = cargo;
	}
	public double getComision() {
		return comision;
	}
	public void setComision(double comision) {
		this.comision = comision;
	}
	public String getCx() {
		return cx;
	}
	public void setCx(String cx) {
		this.cx = cx;
	}
	public String getCy() {
		return cy;
	}
	public void setCy(String cy) {
		this.cy = cy;
	}
	@Override
	public String toString(){
		
		String result = "idBitacoraProsa: "+ this.idBitacoraProsa +
						", idBitacora: " + this.idBitacora +
						", idUsuario: " + this.idUsuario + 
						", tarjeta: " + this.tarjeta +
						", transaccion: " + this.transaccion +
						", autorizacion: "+ this.autorizacion +
						", fecha: " + this.fecha +
						", bit_hora: " + this.bit_hora +
						", concepto: " + this.concepto +
						", cargo: " + this.cargo +
						", comision: " + this.comision +
						", cx: " + this.cx +
						", cy: " + this.cy;
		return result;
	}
		
}
