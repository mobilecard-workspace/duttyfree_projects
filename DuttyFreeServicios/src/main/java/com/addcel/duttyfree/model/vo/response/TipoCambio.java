package com.addcel.duttyfree.model.vo.response;

import java.io.Serializable;

public class TipoCambio implements Serializable{
	private Double conversionRate;
	private String message;
	private int code;
	
	public Double getConversionRate() {
		return conversionRate;
	}
	public void setConversionRate(Double conversionRate) {
		this.conversionRate = conversionRate;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	
	
}
