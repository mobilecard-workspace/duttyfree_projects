package com.addcel.duttyfree.model.vo.response;

import java.io.Serializable;

import com.addcel.duttyfree.model.vo.UserVO;

public class UserFoundVO extends UserVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String desc_tienda;
	private String desc_status;
	private String desc_role;

	public String getDesc_tienda() {
		return desc_tienda;
	}

	public void setDesc_tienda(String desc_tienda) {
		this.desc_tienda = desc_tienda;
	}

	public String getDesc_status() {
		return desc_status;
	}

	public void setDesc_status(String desc_status) {
		this.desc_status = desc_status;
	}

	public String getDesc_role() {
		return desc_role;
	}

	public void setDesc_role(String desc_role) {
		this.desc_role = desc_role;
	}
}