package com.addcel.duttyfree.model.vo.request;

import java.io.Serializable;

public class RequestCatTienda implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String proveedor;

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
}
