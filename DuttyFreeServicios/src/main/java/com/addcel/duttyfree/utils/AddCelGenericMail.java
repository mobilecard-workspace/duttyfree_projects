package com.addcel.duttyfree.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.duttyfree.model.vo.CorreoVO;
import com.addcel.duttyfree.model.vo.response.DetalleVO;
import com.addcel.utils.Utilerias;

public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	private static final String urlString = "http://50.57.192.214:8080/MailSenderAddcel/enviaCorreoAddcel";
	//private static final String urlString = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	private static final String HTML_DOBY = 
				"<!DOCTYPE html> <html><head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\">  body { background: none repeat scroll 0 0 #FFFFFF; font-family: 'Open Sans', sans-serif; font-size: 14px;  } </style> </head> <body data-twttr-rendered=\"true\"> <div style=\"width: 680px; margin: 0 auto\"> <table> <tbody> <tr> <td><img src=\"cid:identifierCID00\"></td> <td></td> <!-- <td><p style=\"font-size: 16px\">La Riviera</p> <p style=\"font-size: 14px\">Duty Free</p></td> --> </tr> </tbody> </table> " +
				"<p style=\"font-weight: bold; font-size: 16px\">Estimado Usuario.</p> <br> <p> Usted ha realizado un Pago Bancario en <span style=\"font-size: 16px; font-weight: bold;\">La Riviera - Duty Free</span>. </p> <p>Con los siguientes datos:</p> <p></p> " +
				"<table> <tbody> <tr> <td>Nombre:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#NOMBRE#></span></td> </tr> <tr> <td>Pasaporte:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#PASAPORTE#></span></td> </tr> <tr> <td>F. Nacimiento:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#FNACIMIENTO#></span></td> </tr> <tr> <td>Nacionalidad:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#NACIONALIDAD#></span></td> </tr> <tr> <td>Vuelo:</td> " +
				"<td><span style=\"font-size: 16px; font-weight: bold;\"><#VUELO#></span></td> </tr> <tr> <td>Producto:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#PRODUCTO#></span></td> </tr> <tr> <td>Monto:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#MONTO#></span></td> </tr> <tr> <td>Moneda:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#MONEDA#></span></td> </tr> <tr> <td>Fecha:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#FECHA#></span></td> </tr> " +
				"<tr> <td>Autorizacion Bancaria:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#AUTBAN#></span></td> </tr> <tr> <td>Referencia:</td> <td><span style=\"font-size: 16px; font-weight: bold;\"><#REFE#></span></td> </tr> </tbody> </table> <br> <br> <p style=\"font-size: 14px\">Nota. Este correo es de carácter informativo, no es necesario que responda al mismo.</p> <p style=\"font-size: 14px\"><b>Gracias por usar MobileCard.</b></p> </div>  </body> </html>";

	public static CorreoVO generatedMail(DetalleVO detalleVO){
		logger.info("Genera objeto para envio de mail: " + detalleVO.getEmail());
		
		CorreoVO correo = new CorreoVO();
		try{
			
	//		String[] attachments = {src_file};
			String body = HTML_DOBY.toString();
			body = body.replaceAll("<#NOMBRE#>", detalleVO.getNombre() != null ? detalleVO.getNombre() : "");
			body = body.replaceAll("<#PASAPORTE#>", detalleVO.getPasaporte() != null ? detalleVO.getPasaporte() : "");
			body = body.replaceAll("<#FNACIMIENTO#>", detalleVO.getFecha_nacimiento() != null ? detalleVO.getNacionalidad() : "");
			body = body.replaceAll("<#NACIONALIDAD#>", detalleVO.getNacionalidad() != null ? detalleVO.getNacionalidad() : "");
			body = body.replaceAll("<#VUELO#>", detalleVO.getNumero_vuelo() != null ? detalleVO.getNumero_vuelo() : "");
			body = body.replaceAll("<#PRODUCTO#>", detalleVO.getDescProducto() != null ? detalleVO.getDescProducto() : "");
			body = body.replaceAll("<#MONTO#>", Utilerias.formatoImporteMon( detalleVO.getTotal()));
			body = body.replaceAll("<#MONEDA#>", (detalleVO.getMoneda()!= null && "1".equalsIgnoreCase(detalleVO.getMoneda())?"MXN":"DLL"));
			body = body.replaceAll("<#FECHA#>", detalleVO.getFecha()!= null ? detalleVO.getFecha().substring(0, 19) : "");
			body = body.replaceAll("<#AUTBAN#>", detalleVO.getReferencia()!= null ? detalleVO.getReferencia() : "");
			body = body.replaceAll("<#REFE#>", detalleVO.getIdBitacora()!= null ? detalleVO.getIdBitacora() : "");
			
			String from = "no-reply@addcel.com";
			String subject = "Acuse Pago Duty Free - Referencia: " + detalleVO.getReferencia();
			String[] to = {detalleVO.getEmail()};
			String[] cid = {"/usr/java/resources/images/DuttyFree/logo-la-riviera.jpg"};
	
	//		correo.setAttachments(attachments);
			correo.setBcc(new String[]{});
			correo.setCc(new String[]{});
			correo.setCid(cid);
			correo.setBody(body);
			correo.setFrom(from);
			correo.setSubject(subject);
			correo.setTo(to);
			
//			sendMail(correo);
//			logger.info("Fin proceso de envio email");
		}catch(Exception e){
			correo = null;
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return correo;
	}

	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			logger.info("data = " + data);

			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
