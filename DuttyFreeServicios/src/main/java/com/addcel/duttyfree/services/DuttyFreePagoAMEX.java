package com.addcel.duttyfree.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.duttyfree.model.mapper.BitacorasMapper;
import com.addcel.duttyfree.model.mapper.DuttyFreeMapper;
import com.addcel.duttyfree.model.vo.DuttyFreeDetalleVO;
import com.addcel.duttyfree.model.vo.RespuestaAmexVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraVO;
import com.addcel.duttyfree.model.vo.pagos.TransactionProcomVO;
import com.addcel.duttyfree.model.vo.request.DatosPago;
import com.addcel.duttyfree.model.vo.response.DetalleVO;
import com.addcel.duttyfree.utils.AddCelGenericMail;
import com.addcel.duttyfree.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;

@Service
public class DuttyFreePagoAMEX {
	
	private static final Logger logger = LoggerFactory.getLogger(DuttyFreePagosService.class);
	private static final String urlStringAMEX = "http://localhost:8080/AmexWeb/AmexAuthorization";
	
	@Autowired
	private BitacorasMapper mapper;
	@Autowired
	private UtilsService utils;
	@Autowired
	private DuttyFreeMapper mapperDF;
	
	private static final String afiliacion = "";
	
	public String procesaPago(String dataEncryp) {
		RespuestaAmexVO respuestaAmex = new RespuestaAmexVO();
		List<DetalleVO> detalleVO = null;
		TransactionProcomVO tp = new TransactionProcomVO();
		
		DatosPago pagoAmex = (DatosPago) utils.jsonToObject(AddcelCrypto.decryptSensitive(dataEncryp), DatosPago.class);
//		pagoAmex.setToken(AddcelCrypto.decryptHard(pagoAmex.getToken()));
		long idBitacora = 0;
		
		if(pagoAmex.getToken() == null){
			respuestaAmex.setCode("1");
			respuestaAmex.setDsc("El parametro TOKEN no puede ser NULL");
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			pagoAmex.setToken(AddcelCrypto.decryptHard(pagoAmex.getToken()));
			logger.info("token ==> {}",pagoAmex.getToken());
			
			if((mapperDF.difFechaMin(pagoAmex.getToken())) > 30){
	//		if(false){
				respuestaAmex.setCode("2");
				respuestaAmex.setDsc("La Transacción ya no es válida");
				logger.info("La Transacción ya no es válida");
			}else{
				
				Double comision = calculaComision(pagoAmex.getTotal());
				idBitacora = insertaBitacoras(pagoAmex, comision);
				
				try {
//					String producto = pagoAmex.getMoneda().equals("1") ? "DutyFree1" : "DutyFree2";
					
					String data = "tarjeta=" + URLEncoder.encode(pagoAmex.getTarjeta(), "UTF-8") +
							"&vigencia=" + URLEncoder.encode(pagoAmex.getVigencia().replaceAll("\\/", ""), "UTF-8") +
							"&monto=" + URLEncoder.encode(String.valueOf(pagoAmex.getTotal()), "UTF-8") +
							"&cid=" + URLEncoder.encode(pagoAmex.getCvv2(), "UTF-8") + 
							"&direccion=" + URLEncoder.encode(pagoAmex.getDireccion(), "UTF-8") +
							"&cp=" + URLEncoder.encode(pagoAmex.getCp(), "UTF-8") +
							"&afiliacion=" + URLEncoder.encode(afiliacion, "UTF-8");
					
					logger.info("Envio de datos AMEX: {}", data);
					
					URL url = new URL(urlStringAMEX);
					URLConnection urlConnection = url.openConnection();
					
					urlConnection.setDoOutput(true);
					
					OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
					wr.write(data);
					wr.flush();
					logger.info("Datos enviados, esperando respuesta de AMEX");
					
					BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
					String line;
					StringBuilder sb = new StringBuilder();
					
					while ((line = rd.readLine()) != null) {
						sb.append(line);
					}
					
					wr.close();
					rd.close();
					
					respuestaAmex = (RespuestaAmexVO) utils.jsonToObject(sb.toString(), RespuestaAmexVO.class);
					
					
					logger.info("Respuesta: {}", sb.toString());
					logger.info("*****************CODIGO AMEX {}", respuestaAmex.getCode());
					if (respuestaAmex.getCode().equals("000")) { //Pago realizado con �xito
						logger.info("Pago realizado con Exito.");
						pagoAmex.setStatus(1);
						pagoAmex.setMsg("EXITO PAGO AMEX FREE DUTTY");
						pagoAmex.setNoAutorizacion(respuestaAmex.getCode());
						respuestaAmex.setTransaction(respuestaAmex.getTransaction());
						updateBitacoras( pagoAmex);
						
						detalleVO = mapperDF.getDetalle(null, null, null, null, null, null, String.valueOf(pagoAmex.getIdBitacora()));
						if(detalleVO != null && detalleVO.size() >0){
							detalleVO.get(0).setTotal(Utilerias.formatoImporteMon( pagoAmex.getTotal()));
							detalleVO.get(0).setReferencia(respuestaAmex.getTransaction());
							detalleVO.get(0).setIdBitacora(pagoAmex.getIdBitacora() + "");
							detalleVO.get(0).setEmail(pagoAmex.getEmail());
							
							AddCelGenericMail.sendMail(
									utils.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
						}else{
							logger.info("No hay datos en la consulta de detalle");
						}
						
						
					} else {
						logger.info("El pago no se pudo realizar error {}", respuestaAmex.getDsc()!=null?respuestaAmex.getDsc():respuestaAmex.getErrorDsc());
						pagoAmex.setStatus(2);
						pagoAmex.setMsg("DECLINADA PAGO AMEX FREE DUTTY ");
						updateBitacoras( pagoAmex);
					}
					
				} catch (Exception ex) {
					logger.error("Error al procesar pago AMEX: {}", ex);
					logger.info("El pago no se pudo realizar error {}", ex);
					pagoAmex.setStatus(2);
					pagoAmex.setMsg("ERROR PAGO AMEX FREE DUTTY");
					respuestaAmex.setCode("002");
					respuestaAmex.setDsc("Ocurrio un error durante la transacci�n: " + ex.getMessage());
					updateBitacoras( pagoAmex);
				}
			}
		}
		return AddcelCrypto.encryptSensitive("12345678", utils.objectToJson(respuestaAmex));
	}
	
	
	private long insertaBitacoras(DatosPago dp, Double comision ){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getIdUser(), "PAGO DUTTY FREE AMEX", String.valueOf(dp.getTotal()), dp.getImei(), "PAGO DUTTY FREE AMEX",
				AddcelCrypto.encryptTarjeta(dp.getTarjeta()),dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		mapper.addBitacora(tb);
		TBitacoraProsaVO tbp = new TBitacoraProsaVO(tb.getIdBitacora(), Long.parseLong(dp.getIdUser()), "PAGO DUTTY FREE AMEX",
				dp.getTotal(), 0.00, dp.getCx(), dp.getCy());
		mapper.addBitacoraProsa(tbp);
		dp.setIdBitacora(tb.getIdBitacora());
		DuttyFreeDetalleVO dfd = new DuttyFreeDetalleVO(
				tb.getIdBitacora(), dp.getIdUser(),dp.getDescProducto(), dp.getTotal(), dp.getMoneda(), 
				0.0d, dp.getNacionalidad(),	dp.getPasaporte(), dp.getNombre_completo(), 
				dp.getFecha_nacimiento(), dp.getNumero_vuelo(), dp.getEmail(), comision);
		mapper.addDfDetalle(dfd);
		return tb.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		
		tb.setIdBitacora(dp.getIdBitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		mapper.updateBitacora(tb);
		tbp.setIdBitacora(dp.getIdBitacora());
		tbp.setAutorizacion(dp.getNoAutorizacion());
		if(tbp.getAutorizacion()!=null)
			mapper.updateBitacoraProsa(tbp);
		// idStatus
		mapper.updateDfDetalle(dp.getIdBitacora(), dp.getStatus());
	}
	
	private Double calculaComision(Double total){
		// Obtener parametros
		Double comision = null;
		try{
			refreshParams();			
			Double comisionFija = (Double)lookupReference("@RIVERACOMISION");
			if(comisionFija == null){
				comisionFija = new Double(mapperDF.getParametro("@RIVERACOMISION"));
				insertReference("@RIVERACOMISION", comisionFija);
			}
			Double comisionPorcentaje = (Double) lookupReference("@RIVERACOMISIONPOR");
			if(comisionPorcentaje == null){
				comisionPorcentaje = new Double(mapperDF.getParametro("@RIVERACOMISIONPOR"));
				insertReference("@RIVERACOMISIONPOR", comisionPorcentaje);
			}
			Double minCom = (Double)lookupReference("@RIVERAMINCOM");
			if(minCom == null){
				minCom = new Double(mapperDF.getParametro("@RIVERAMINCOM"));
				insertReference("@RIVERAMINCOM", minCom);
			}
			
			Double minComPorcentaje = (Double)lookupReference("@RIVERAMINCOMPOR");
			if(minComPorcentaje == null){
				minComPorcentaje = new Double(mapperDF.getParametro("@RIVERAMINCOMPOR"));
				insertReference("@RIVERAMINCOMPOR", minComPorcentaje);
			}

			logger.info("Parametos: \nTotal = " + total + "\ncomisionFija = " + comisionFija + "\ncomisionPorcentaje = "+ comisionPorcentaje 
					+ "\nminCom = " + minCom + "\nminComPorcentaje = "+ minComPorcentaje);	
			if(total.doubleValue() >= minComPorcentaje.doubleValue() && total.doubleValue() <= minCom.doubleValue() )

				comision=total*comisionPorcentaje;
			else										
				comision = comisionFija;		
			
		} catch(Exception e){
			logger.error("Error al calcular comision: " + e.getMessage());
		}
		logger.info("Comision calculada: " + comision);
		return comision;
	}
	private Object lookupReference(String jndi)
    {
		logger.info("inicio lookupReference:");
        InitialContext contexto;
        Object obj;
        try {
        	logger.info("Buscando en el contexto :"+jndi);
            contexto = new InitialContext();
            obj = contexto.lookup(jndi);
            logger.info(jndi+" Encontrado, del tipo "+obj.getClass().getName());
        } catch (NamingException e) {
        	logger.info(jndi+ " no esta en el contexto");                       
            obj = null;
        }       
        logger.info("Fin lookupReference:");
        return obj;
    }
    private void removeReference(String jndi) 
    {
    	logger.info("inicio removeReference:");
        InitialContext contexto;
        try {
            contexto = new InitialContext();
            contexto.unbind(jndi);
            contexto.removeFromEnvironment(jndi);
        } catch (NamingException e) {
        	logger.info("Fin removeReference, error: " + e.getMessage());   
        }
        logger.info("Fin removeReference:");       
    }
 
    private void insertReference(String jndi, Object obj) throws Exception
    {
    	logger.info("inicio insertReference:");
        InitialContext contexto;
        contexto = new InitialContext();
        if(lookupReference(jndi)==null){
        	logger.info("insertando en el contexto: "+ jndi);
            contexto.addToEnvironment(jndi,obj);
            contexto.bind(jndi,obj);
        } else
        {
        	logger.info("actualizando en el contexto : "+ jndi);
            contexto.rebind(jndi,obj);
        }
        logger.info("fin insertReference:");
    }
    private void refreshParams(){
    	
    	try{
    		SimpleDateFormat formato = new SimpleDateFormat("ddMMyyyy");
    		Calendar cal =  GregorianCalendar.getInstance();
    		Date fecha = cal.getTime();
    		String hoy = formato.format(fecha);
    		String fechaVigencia = (String)lookupReference("fechaVigencia");
    		logger.info("Hoy:" + hoy);
    		logger.info("Fecha vigencia parametros contexto:" + fechaVigencia);
    		if(fechaVigencia!=null){
    			if(!hoy.equals(fechaVigencia)){
    				removeReference("@RIVERACOMISION");
    				removeReference("@RIVERACOMISIONPOR");
    				removeReference("@RIVERAMINCOM");
    				removeReference("@RIVERAMINCOMPOR");
    				removeReference("fechaVigencia");
    				insertReference("fechaVigencia", hoy);
    			}
    		}else
    			insertReference("fechaVigencia", hoy);
    	}catch(Exception e){
    		logger.error("Error al refrescar los parametros: "+ e.getMessage());
    	}
    }
	public RespuestaAmexVO procesaPagoProv(String Tarjeta, String Vigencia, String Monto, String Cvv, String Direccion, String Cp ) throws IOException {
		RespuestaAmexVO respuestaAmex = new RespuestaAmexVO();

		String data = "tarjeta=" + URLEncoder.encode(Tarjeta, "UTF-8") +
					  "&vigencia=" + URLEncoder.encode(Vigencia.replaceAll("\\/", ""), "UTF-8") +
					  "&monto=" + URLEncoder.encode(Monto, "UTF-8") +
					  "&cid=" + URLEncoder.encode(Cvv, "UTF-8") + 
				      "&direccion=" + URLEncoder.encode(Direccion, "UTF-8") +
				      "&cp=" + URLEncoder.encode(Cp, "UTF-8");
					
		logger.info("Envio de datos AMEX: {}", data);
					
		URL url = new URL(urlStringAMEX);
		URLConnection urlConnection = url.openConnection();
					
		urlConnection.setDoOutput(true);
					
		OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
		wr.write(data);
		wr.flush();
		logger.info("Datos enviados, esperando respuesta de AMEX");
					
		BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String line;
		StringBuilder sb = new StringBuilder();
				
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
				
		wr.close();
		rd.close();
			
		respuestaAmex = (RespuestaAmexVO) utils.jsonToObject(sb.toString(), RespuestaAmexVO.class);
		
		return respuestaAmex;
	}

}