package com.addcel.duttyfree.services;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.addcel.duttyfree.model.mapper.DuttyFreeMapper;
import com.addcel.duttyfree.model.vo.AbstractResponse;
import com.addcel.duttyfree.model.vo.ComisionVO;
import com.addcel.duttyfree.model.vo.CorreoVO;
import com.addcel.duttyfree.model.vo.LoginVO;
import com.addcel.duttyfree.model.vo.RoleVO;
import com.addcel.duttyfree.model.vo.TiendaVO;
import com.addcel.duttyfree.model.vo.UserVO;
import com.addcel.duttyfree.model.vo.request.RequestBusquedaPagos;
import com.addcel.duttyfree.model.vo.request.RequestCatRoles;
import com.addcel.duttyfree.model.vo.request.RequestCatTienda;
import com.addcel.duttyfree.model.vo.request.RequestFindUser;
import com.addcel.duttyfree.model.vo.request.RequestVendedor;
import com.addcel.duttyfree.model.vo.response.DetalleVO;
import com.addcel.duttyfree.model.vo.response.LoginRespuesta;
import com.addcel.duttyfree.model.vo.response.TipoCambio;
import com.addcel.duttyfree.utils.AddCelGenericMail;
import com.addcel.duttyfree.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Service
public class DuttyFreeService {
	private static final Logger logger = LoggerFactory
			.getLogger(DuttyFreeService.class);
	private static final String URL_ENVIO_MAIL="http://50.57.192.214:8080/MailSenderAddcel/enviaCorreoAddcel";
	//private static final String URL_ENVIO_MAIL="http://50.57.192.210:8080/MailSenderAddcel/enviaCorreoAddcel";
	private static final String KEY_PASSWORD="1234567890ABCDEF0123456789ABCDEF";
	@Autowired
	private DuttyFreeMapper mapper;
	@Autowired
	private UtilsService utilService;	
	
	public String addUser(String jsonEnc) {
		AbstractResponse res = new AbstractResponse();
		String jsonResEnc=null;
		res.setIdError(1);		
		try {
			String json = AddcelCrypto.decryptHard(jsonEnc);
			UserVO usuario = (UserVO) utilService.jsonToObject(json, UserVO.class);
			usuario.setPassword(generaPassword());
			int id = mapper.addUser(usuario);			
				mapper.insertaModulosUsuario(usuario.getIdUser(), usuario.getIdRole());
				enviaPasswordMail(usuario.getPassword(),usuario.getEmail());
			res.setIdError(0);
			res.setMensajeError("Usuario guardado correctamente");
			
			logger.info("Usuario guardado correctamente con rs ==> {} id ==> {} ", id,usuario.getIdUser());
		} catch (DuplicateKeyException pe) {				
			res.setMensajeError("El login de usuario ya existe");
			logger.error("Nombre de usuario duplicado: {}", pe.getMessage());
		}catch (Exception pe) {			
			res.setMensajeError("Error al registrar usuario");
			logger.error("Error al insertar usuario: {}", pe.getMessage());
		}finally{
			jsonResEnc=utilService.objectToJson(res);
			jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
		}
		return jsonResEnc;
	}
	
	public String updateUser(String jsonEnc){				
		//*******************************
		String prueba="{\"idUser\":0,\"nombres\":\"nombre completo actualizado\",\"apellidoP\":\"apellido paterno act\",\"apellidoM\":\"apellido materno act\",\"email\":\"gaidar26@gmail.com\",\"telefono\":\"1234567899\",\"idUserEmpresa\":1,\"idTienda\":1,\"password\":null,\"lastLogin\":null,\"fechaAlta\":null,\"idSupervisor\":1,\"login\":\"login user\",\"loginCount\":null,\"idStatus\":0,\"idRole\":1}";		
		AddcelCrypto.encryptHard(prueba);
		//******************************
		
		AbstractResponse res=new AbstractResponse();
		String json=AddcelCrypto.decryptHard(jsonEnc);
		
		String jsonResEnc=null;
		UserVO usuario=(UserVO) utilService.jsonToObject(json, UserVO.class);
		if(usuario.getIdUser()!=usuario.getIdSupervisor()){
		try{
		int rs=mapper.updateUser(usuario);		
		res.setIdError(0);
		res.setMensajeError("Actualizaci�n correcta");
		logger.info("Usuario actualizado: id ==> {} regAct==> {}",usuario.getIdUser(),rs);
		}catch(Exception e){
			res.setIdError(1);
			res.setMensajeError("Error al actualizar Usuario");
			logger.error("Error al actualizar usuario con id ==> {}. Error\n {}",usuario.getIdUser(),e);
		}						
		}else{
			res.setIdError(2);
			res.setMensajeError("Operacíon no permitida");
		}
		jsonResEnc=utilService.objectToJson(res);
		jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
		return jsonResEnc;
	}

	public String login(String jsonEnc) {
		LoginRespuesta lr = null;
		String jsonResEnc=null;
		String json = AddcelCrypto.decryptHard(jsonEnc);
		
		LoginVO login = (LoginVO) utilService.jsonToObject(json, LoginVO.class);				
		login.setPassword(encriptaPassword(login.getPassword()));
		UserVO user = mapper.userIntentosLogin(login.getLogin());			    
		if(user!=null){			
			int status=user.getIdStatus();
			if(status==1){								
					if(user.getPassword().equals(login.password)){
						lr = mapper.loginUsuario(user.getIdUser());
						mapper.updateIntentosStatus(user.getIdUser(), 0, "contadorLogin");//Reset loginCount=0
						
						//Revisar bien en que condiciones se manda el tipo de cambio						
						Double tcs = mapper.getTipoCambio(18); 
						logger.info("====> {}",tcs);
						if(tcs!=null)
							lr.setTipoCambio(tcs);
						else 
							logger.debug("soy nulo");
						
						lr.setIdError(0);
						lr.setMensajeError("Usuario v�lido");
						logger.info("Login del usuario con id ==> {}",lr.getIdUser());
					}else{												
						int contador=user.getLoginCount()+1;						
						lr = new LoginRespuesta();
						if(user.getIdTienda() <= contador){//idTienda trae el numero maximo de intentos por rol
							logger.debug("intentos ==> {} = max_login ==> {}",contador,user.getIdTienda());
							//bloquear usuario
							mapper.updateIntentosStatus(user.getIdUser(), 3, "status");//Status 3=bloqueado
							mapper.updateIntentosStatus(user.getIdUser(), 0, "contador");//Reset loginCount=0							    
							lr.setIdError(4);
							lr.setMensajeError("El usuario ha sido bloqueado");
							logger.info("++++++++++++++++ Bloqueo de usuario DuttyFree +++++++++++++++++++");
							logger.info("# de intentos superado. El usuario con id ==> {} ha sido bloqueado ",user.getIdUser());
						}else{							
							mapper.updateIntentosStatus(user.getIdUser(), contador, "contador");//incrementa el contador							
							lr.setIdError(5);
							lr.setMensajeError("Contraseña incorrecta");							
						}
					}									
			}else if(status==2){
				lr = new LoginRespuesta();
				lr.setIdError(2);
				lr.setIdUser(user.getIdUser());
				lr.setMensajeError("Se requiere cambio de contrase�a");
				logger.info("Se requiere cambio de contrase�a del usuario con id ==> {}",user.getIdUser());
			}else if(status==3){
				lr = new LoginRespuesta();
				lr.setIdError(3);
				lr.setMensajeError("Usuario bloqueado");
				logger.info("Usuario bloqueado id ==> {}",user.getIdUser());
			}else if(status==4){
				lr = new LoginRespuesta();
				lr.setIdError(4);
				lr.setMensajeError("Usuario dado de baja, favor de contactar con su Supervisor.");
				logger.info("Usuario dado de baja id ==> {}",user.getIdUser());
			}else{
				lr = new LoginRespuesta();
				lr.setIdError(4);
				lr.setMensajeError("Status no valido");
				logger.info("Status no valido id ==> {}",user.getIdUser());
			}
		} else{
			lr = new LoginRespuesta();
			lr.setIdError(1);
			lr.setMensajeError("El usuario no existe");
			logger.error("Error al realizar login-usuario: {} ==> {}",
					login.getLogin(), login.getPassword());
		}	
			jsonResEnc=utilService.objectToJson(lr);
			jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
	
		return jsonResEnc;		
	}
	
	public String listaTiendas(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestCatTienda rct = (RequestCatTienda) utilService.jsonToObject(json, RequestCatTienda.class);
		List<TiendaVO> tiendas = mapper.listaTiendas(rct.getProveedor());
		dataBack = utilService.objectToJson(tiendas);
		dataBack = AddcelCrypto.encryptHard(dataBack);		
		return dataBack;
	}
	
	public String listaRoles(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestCatRoles rcr = (RequestCatRoles)utilService.jsonToObject(json, RequestCatRoles.class);
		List<RoleVO> roles = mapper.getCatRoles(rcr.getId_aplicacion(), rcr.getId_proveedor());
		dataBack = utilService.objectToJson(roles);
		dataBack = AddcelCrypto.encryptHard(dataBack);		
		return dataBack;
	}
	
	public String findUser(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestFindUser request = (RequestFindUser) utilService.jsonToObject(json, RequestFindUser.class);
		request.setLogin(request.getLogin() != null && !"".equals( request.getLogin() )? request.getLogin() + "%": request.getLogin());
		request.setNombres(request.getNombres() != null && !"".equals( request.getNombres() )? request.getNombres() + "%": request.getNombres());
		request.setPaterno(request.getPaterno() != null && !"".equals( request.getPaterno() )? request.getPaterno() + "%": request.getPaterno());
		request.setMaterno(request.getMaterno() != null && !"".equals( request.getMaterno() )? request.getMaterno() + "%": request.getMaterno());
		dataBack = utilService.objectToJson(mapper.findUser(request.getNombres(), request.getPaterno(), request.getMaterno(), request.getLogin(),null,null));
		dataBack = AddcelCrypto.encryptHard(dataBack);
		return dataBack;
	}
	
	public String getVendor(String data) {
		String dataBack = "";
		String json = AddcelCrypto.decryptHard(data);
		RequestVendedor request = (RequestVendedor) utilService.jsonToObject(json, RequestVendedor.class);
		dataBack = utilService.objectToJson(mapper.findUser(null,null,null,null, request.getId_supervisor(), request.getId_tienda()));
		return AddcelCrypto.encryptHard(dataBack);
	}
	
	public String generaToken(){				
		String token=AddcelCrypto.encryptHard(mapper.getFechaActual());
		String json="{\"token\":\""+token+"\"}";
		json=AddcelCrypto.encryptHard(json);		
		return json;
	}

	public String deleteUser(String jsonEnc){
		AbstractResponse res=new AbstractResponse(1, "Error al borrar usuario");
		String json=AddcelCrypto.decryptHard(jsonEnc);
		UserVO user = (UserVO) utilService.jsonToObject(json, UserVO.class);
		if(user.getIdUser()==user.getIdSupervisor()){
			res.setMensajeError("Acci�n no permitida");
			logger.info("Se trato de borrar el mismo usuario: {}",user.getIdUser(), user.getIdSupervisor());
		}else{			
			mapper.updateIntentosStatus(user.getIdUser(), 4, "status");
			res.setIdError(0);
			res.setMensajeError("Usuario borrado correctamente");
			logger.info("Se borro el usuario con id ==> {}, editado por usuario con id ==> {}",user.getIdUser(), user.getIdSupervisor());
		}	
		String jsonResEnc=utilService.objectToJson(res);
		jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);
		return jsonResEnc;
	}
	
	public String actualizaPassword(String jsonEnc,int status){
		/*String actPass="{\"idUser\":14,\"password\":\"cadena password\",\"idSupervisor\":14}";
		String deleteUser="{\"idUser\":14,\"idSupervisor\":1}";
		AddcelCrypto.encryptHard(actPass);
		deleteUser=AddcelCrypto.encryptHard(deleteUser);
		AddcelCrypto.decryptHard(deleteUser);*/
		String json=AddcelCrypto.decryptHard(jsonEnc);
		UserVO user = (UserVO)utilService.jsonToObject(json, UserVO.class);
		if(status==1){
			user.setIdStatus(1);//Cambio de cntraseña de usuario nuevo
		}
		AbstractResponse ar=new AbstractResponse();
//		if(user.getIdUser()==user.getIdSupervisor()){
			user.setPassword(encriptaPassword(user.getPassword()));
			mapper.updateUser(user);
			ar.setIdError(0);
			ar.setMensajeError("Actualizacion correcta");
//		}else{
//			ar.setIdError(1);
//			ar.setMensajeError("Operacion no permitida");
//		}
		String jsonResEnc=utilService.objectToJson(ar);
		jsonResEnc=AddcelCrypto.encryptHard(jsonResEnc);		
		return jsonResEnc;
	}

	public String generaPassword() {
		String kk = parsePass(KEY_PASSWORD);				
		String sPwd = (random(5869452) + random(5869452) + random(5869452)+ random(5869452))
				.substring(0, 10);
		logger.info("Password: {}",sPwd);
		sPwd =Crypto.aesEncrypt(kk, sPwd);				
		logger.info("Password enc: {}",sPwd);		
		return sPwd;
	}
	
	public String encriptaPassword(String password){
		String kk = parsePass(KEY_PASSWORD);
		logger.debug("password: {}",password);
		password =Crypto.aesEncrypt(kk, password);		
		logger.info("Password enc: {}",password);
		return password;
	}

	public String random(int n) {
		java.util.Random rand = new java.util.Random();
		int x = rand.nextInt(n);
		return "" + x;
	}		

	private String parsePass(String pass) {
		int len = pass.length();
		String key = "";

		for (int i = 0; i < 32 / len; i++) {
			key += pass;
		}

		int carry = 0;
		while (key.length() < 32) {
			key += pass.charAt(carry);
			carry++;
		}
		return key;
	}
	
	private void enviaPasswordMail(String psw,String mailTo){
		CorreoVO correo = new CorreoVO();		
		String kk = parsePass(KEY_PASSWORD);		
		correo.setBody("Contraseña: " + Crypto.aesDecrypt(kk,psw));
		correo.setTo(new String []{mailTo});
		correo.setSubject("Contraseña Dutty Free");
		String json=utilService.objectToJson(correo);
		logger.debug("Json correo: {}",json);		
		try {
			URL url = new URL(URL_ENVIO_MAIL);			
			HttpURLConnection con = (HttpURLConnection) url.openConnection();	
			// Se indica que se escribira en la conexi�n
			con.setDoOutput(true);
			con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			con.setRequestMethod("POST");			
			
			// Se escribe los parametros enviados a la url
			OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());						
			wr.write(json);
			wr.close();					
			logger.info("Codigo de respuesta MailSenderAddcel: {}",con.getResponseCode());				
			con.disconnect();			
		} catch (MalformedURLException ex) {
			logger.error("Error MalformedURLException:"+ex);
		} catch (IOException ex) {
			logger.error("Error IOException:"+ex);
		}
	}
	
	public String busquedaPagos(String data) {
		RequestBusquedaPagos request = 
				(RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
		return AddcelCrypto.encryptHard(utilService.objectToJson(
				mapper.getDetalle(null, request.getIdUser(), null, request.getFechaIni(), request.getFechaFin(), request.getIdRol(),null)));
	}
	
	public String reenvioRecibo(String data) {
		List<DetalleVO> detalleVO = null;
		RequestBusquedaPagos request = null;
		String json = null;
		try{
			request = (RequestBusquedaPagos) utilService.jsonToObject(AddcelCrypto.decryptHard(data), RequestBusquedaPagos.class);
			if(request.getIdUser() == null){
				json = "{\"idError\":2,\"mensajeError\":\"Falta el parametro idUsuario.\"}";
			}else if(request.getIdBitacora() == null){
				json = "{\"idError\":3,\"mensajeError\":\"Falta el parametro idBitacora.\"}";
			}else{
				
				detalleVO =  mapper.getDetalle(null, request.getIdUser(), null, null, null, null, request.getIdBitacora());
				
				if(detalleVO != null && detalleVO.size() >0){
					logger.info("Se obtuvieron datos, inicia proceso de reenvio de correo.");
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
					json = "{\"idError\":0,\"mensajeError\":\"Recibo reenviado correctamente.\"}";
				}else{
					logger.error("ID_BITACORA: " + request.getIdBitacora() + ", No se obtuvieron datos para inicia proceso de reenvio del recibo.");
					json = "{\"idError\":1,\"mensajeError\":\"No se obtuvieron datos para inicia el proceso de reenvio del recibo.\"}";
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error al reenviar el recibo: {}", e.getMessage());
			json = "{\"idError\":2,\"mensajeError\":\"Ocurrio un error al reenviar el recibo.\"}";
		}finally{
			json = AddcelCrypto.encryptHard(json);
		}
		return json;	
	}
}
