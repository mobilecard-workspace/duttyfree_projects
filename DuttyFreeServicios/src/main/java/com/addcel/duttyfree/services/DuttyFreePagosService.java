package com.addcel.duttyfree.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.duttyfree.model.mapper.BitacorasMapper;
import com.addcel.duttyfree.model.mapper.DuttyFreeMapper;
import com.addcel.duttyfree.model.vo.ComisionVO;
import com.addcel.duttyfree.model.vo.DuttyFreeDetalleVO;
import com.addcel.duttyfree.model.vo.pagos.ProcomVO;
import com.addcel.duttyfree.model.vo.pagos.ProsaPagoVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.duttyfree.model.vo.pagos.TBitacoraVO;
import com.addcel.duttyfree.model.vo.pagos.TransactionProcomVO;
import com.addcel.duttyfree.model.vo.request.DatosPago;
import com.addcel.duttyfree.model.vo.response.DetalleVO;
import com.addcel.duttyfree.utils.AddCelGenericMail;
import com.addcel.duttyfree.utils.UtilsService;
import com.addcel.duttyfree.model.vo.response.ResponseProcesaProsa;
import com.addcel.utils.AddcelCrypto;

@Service
public class DuttyFreePagosService {
	private static final Logger logger = LoggerFactory.getLogger(DuttyFreePagosService.class);
	
	private static final String URL_AUT_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
//	private static final String URL_AUT_PROSA = "http://50.57.192.213:8080/ProsaWeb/ProsaAuth";
	@Autowired 
	private DuttyFreeMapper mapper;
	@Autowired
	private UtilsService utilService;	
	@Autowired
	private BitacorasMapper bm;
	@Autowired
	private DuttyFreeMapper mapperDF;
	
	private static final String patron = "ddhhmmss";
	private static final String patronCom = "yyyy-MM-dd hh:mm:ss";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);

	public String procesaPagoVisa(String jsonEnc){		
		// prueba
		DatosPago datosPago = null;
		Double total = 0.0;
		Double subtotal = 0.0;
		String json = null;
		
		datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DatosPago.class);
		
		if(datosPago.getToken() == null){
			json = "{\"idError\":1,\"mensajeError\":\"El parametro TOKEN no puede ser NULL\"}";
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
			logger.info("token ==> {}",datosPago.getToken());
			
			if((mapperDF.difFechaMin(datosPago.getToken())) > 30){
//			if(false){
				json = "{\"idError\":2,\"mensajeError\":\"La Transacción ya no es válida\"}";
				logger.info("La Transacción ya no es válida");
			}else{
			
				ComisionVO comisiones = mapper.getComisiones(18);//18 idProveedor Dutty Free
				//Insetar Bitacoras Prosa y detalle						
				if(datosPago.getMoneda().equalsIgnoreCase("1")){	//MXD=1				
					logger.info("****************** MXD ******************");
					total=calculaTotal(comisiones,datosPago.getSubTotal() );
					Double comision = calculaComision(total);
					long idBitacora=insertaBitacoras(datosPago, total, 0d, comision);
					
					datosPago.setIdBitacora(idBitacora);
					
					json = swichAbierto(datosPago,total, comisiones.getAfiliaciones().get("p"));
					
				}else if(datosPago.getMoneda().equalsIgnoreCase("2")){
					logger.info("****************** USD ******************");
									
					if(comisiones.getTipoCambio() == 0){
						//error el valor del tipo de cmabio es 0
						logger.error("No hay configurado un tipo de cambio NO DISPONIBLE.");
						json = "{\"idError\":3,\"mensajeError\":\"No hay configurado un tipo de cambio NO DISPONIBLE\"}";
					}else{
						subtotal = datosPago.getSubTotal() * comisiones.getTipoCambio();
						logger.info("Subtotal USD * tipoCambio = Subtotal MXD: {} * {} = {}",
								new Object[]{datosPago.getSubTotal(),comisiones.getTipoCambio(),subtotal});						
						subtotal = calculaTotal(comisiones,subtotal);
						total = subtotal/comisiones.getTipoCambio();
						Double comision = calculaComision(subtotal);
						long idBitacora = insertaBitacoras(datosPago, total, comisiones.getTipoCambio(),comision);
						datosPago.setIdBitacora(idBitacora);
						
						logger.info("Total MXD / tipoCambio = Total USD: {} / {} = {},",new Object[]{subtotal, comisiones.getTipoCambio(),total});
						
						json = swichAbierto(datosPago,total, comisiones.getAfiliaciones().get("d"));
					}
				}			
				logger.debug("Comisiones: {}",utilService.objectToJson(comisiones));	
				json = AddcelCrypto.encryptSensitive(formato.format(new Date()),json);
			}
		}
		return json;
	}
	
	private String swichAbierto(DatosPago datosPago,Double total, String afiliacion){
		ProsaPagoVO prosaPagoVO = null;
		DetalleVO detalleVO = new DetalleVO();
		String json = null;
		try{
			logger.info("datosPago.getTarjeta(): {}", datosPago.getTarjeta());
			logger.info("datosPago.getVigencia(): {}", datosPago.getVigencia());
			logger.info("datosPago.getNombre_completo(): {}", datosPago.getNombre_completo());
			logger.info("datosPago.getCvv2(): {}", datosPago.getCvv2());
			logger.info("datosPago.getTotal(): {}", total);
			logger.info("datosPago.getMoneda(): {}", datosPago.getMoneda());
			logger.info("afiliacion(): {}", afiliacion);
			
			if(datosPago.getMoneda().trim().equals("2"))
				datosPago.setMoneda("840");
			
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(datosPago.getTarjeta(), "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(datosPago.getVigencia(), "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(datosPago.getNombre_completo(), "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(datosPago.getCvv2(), "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(total + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode(afiliacion, "UTF-8") );
					if(!datosPago.getMoneda().equals("1"))
						data.append( "&moneda="   ).append( URLEncoder.encode(datosPago.getMoneda(), "UTF-8")) ;					
					
			logger.info("Envío de datos VISA: {}", data);
			
			URL url = new URL(URL_AUT_PROSA);
			URLConnection urlConnection = url.openConnection();
			
			urlConnection.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
			wr.write(data.toString());
			wr.flush();
			logger.info("Datos enviados, esperando respuesta de PROSA");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
			wr.close();
			rd.close();
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				detalleVO.setEmail(datosPago.getEmail());
				detalleVO.setDescProducto(datosPago.getDescProducto());
				detalleVO.setTotal(total.toString());
				detalleVO.setMoneda(datosPago.getMoneda());
				detalleVO.setFecha(formatoCom.format(new Date()));
				detalleVO.setReferencia(prosaPagoVO.getAutorizacion());
				detalleVO.setIdBitacora(String.valueOf(datosPago.getIdBitacora()));
				detalleVO.setNombre(datosPago.getNombre_completo());
				detalleVO.setPasaporte(datosPago.getPasaporte());
				detalleVO.setFecha_nacimiento(datosPago.getFecha_nacimiento());
				detalleVO.setNacionalidad(datosPago.getNacionalidad());
				detalleVO.setNumero_vuelo(datosPago.getNumero_vuelo());
				datosPago.setDescRechazo(prosaPagoVO.getDescripcionRechazo());
				datosPago.setNoAutorizacion(prosaPagoVO.getAutorizacion());
				
				
				if(prosaPagoVO.isAuthorized()){
					datosPago.setStatus(1);
					datosPago.setMsg("EXITO PAGO DUTY FREE VISA AUTORIZADA");
					json = "{\"idError\":0,\"mensajeError\":\"El pago fue exitoso.\",\"transaccion\":\"" + prosaPagoVO.getTransactionId() + 
							"\",\"autorizacion\":\"" + prosaPagoVO.getAutorizacion() +
							"\",\"referencia\":\"" + datosPago.getIdBitacora() + "\"}";
					
					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO)));
					
				}else if(prosaPagoVO.isRejected()){
					datosPago.setStatus(3);
					datosPago.setMsg("PAGO DUTY FREE VISA RECHAZADA");
					json = "{\"idError\":4,\"mensajeError\":\"El pago fue rechazado.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"RECHAZADA")));
					
				}else if(prosaPagoVO.isProsaError()){
					datosPago.setStatus(2);
					datosPago.setMsg("PAGO DUTY FREE VISA ERROR");
					json = "{\"idError\":5,\"mensajeError\":\"Ocurrio un error durante el pago Banco.\",\"referencia\":" + prosaPagoVO.getTransactionId() + "}";
					
//					AddCelGenericMail.sendMail(utilService.objectToJson(AddCelGenericMail.generatedMailError(datosPago,"ERROR")));
				}
				
				updateBitacoras(datosPago);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Ocurrio un error durante el pago al banco: {}", e);
			datosPago.setStatus(2);
			datosPago.setMsg("Ocurrio un error durante el pago al banco");
			json = "{\"idError\":6,\"mensajeError\":\"Ocurrio un error durante el pago.\"}";
			updateBitacoras(datosPago);
		}
		return json;
	}
	
	public ProsaPagoVO pagoVisaProb(String tarjeta, String vigencia, String Nombre, String cvv2, String total){
		ProsaPagoVO prosaPagoVO = null;
		try{
			StringBuffer data = new StringBuffer() 
					.append( "card="      ).append( URLEncoder.encode(tarjeta, "UTF-8") )
					.append( "&vigencia=" ).append( URLEncoder.encode(vigencia, "UTF-8") )
					.append( "&nombre="   ).append( URLEncoder.encode(Nombre, "UTF-8") )
					.append( "&cvv2="     ).append( URLEncoder.encode(cvv2, "UTF-8") )
					.append( "&monto="    ).append( URLEncoder.encode(total + "", "UTF-8") )
					.append( "&afiliacion=" ).append( URLEncoder.encode("7442483", "UTF-8") )
					.append( "&moneda="   ).append( URLEncoder.encode("840", "UTF-8")) ;
					
			logger.info("Envío de datos VISA: {}", data);
			
			URL url = new URL(URL_AUT_PROSA);
			URLConnection urlConnection = url.openConnection();
			
			urlConnection.setDoOutput(true);
			
			OutputStreamWriter wr = new OutputStreamWriter(urlConnection.getOutputStream());
			wr.write(data.toString());
			wr.flush();
			logger.info("Datos enviados, esperando respuesta de PROSA");
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
			String line = null;
			StringBuilder sb = new StringBuilder();
			
			while ((line = rd.readLine()) != null) {
				sb.append(line);
			}
			
			wr.close();
			rd.close();
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb.toString(), ProsaPagoVO.class);
			
		}catch(Exception e){
			logger.error("Ocurrio un error durante el pago al banco: {}", e);
		}
		return prosaPagoVO;
	}

	public ProcomVO procesaPago(String jsonEnc){		
		// prueba
		DatosPago datosPago = null;
		ProcomVO procom = null;
		Double total = 0.0;
		Double subtotal = 0.0;
		
		datosPago = (DatosPago) utilService.jsonToObject(AddcelCrypto.decryptSensitive(jsonEnc), DatosPago.class);
		
		if(datosPago.getToken() == null){
			procom = new ProcomVO(1,"El parametro TOKEN no puede ser NULL");
			logger.error("El parametro TOKEN no puede ser NULL");
		}else{
			datosPago.setToken(AddcelCrypto.decryptHard(datosPago.getToken()));
			logger.info("token ==> {}",datosPago.getToken());
			
			if((mapperDF.difFechaMin(datosPago.getToken())) > 30){
//			if(false){
				procom = new ProcomVO(2,"La Transacción ya no es válida");
				logger.info("La Transacción ya no es válida");
			}else{
			
				ComisionVO comisiones = mapper.getComisiones(18);//18 idProveedor Dutty Free
				//Insetar Bitacoras Prosa y detalle						
				if(datosPago.getMoneda().equalsIgnoreCase("1")){	//MXD=1				
					logger.info("****************** MXD ******************");
					total=calculaTotal(comisiones,datosPago.getSubTotal() );
					Double comision = calculaComision(total);
					long idBitacora=insertaBitacoras(datosPago, total, 0d, comision);
					procom = comercioFin(String.valueOf(datosPago.getIdUser()), 
							String.valueOf(idBitacora), 
							Double.toString(total), 
							"0", 
							datosPago.getEmail(),
							comisiones.getAfiliaciones().get("p"),
							"@DuttyFreeVarCurPesos",
							"@DuttyFreeUrlCurPesos");				
				}else if(datosPago.getMoneda().equalsIgnoreCase("2")){
					logger.info("****************** USD ******************");
					if(comisiones.getTipoCambio()!=null&&comisiones.getTipoCambio().doubleValue()!=0){
						Double tipoCambio= comisiones.getTipoCambio().doubleValue();						
						subtotal = datosPago.getSubTotal()*tipoCambio;
						logger.info("Subtotal USD * tipoCambio = Subtotal MXD: {} * {} = {}",new Object[]{datosPago.getSubTotal(),tipoCambio,subtotal});						
						subtotal=calculaTotal(comisiones,subtotal);
						total=subtotal/tipoCambio;
						Double comision = calculaComision(subtotal);
						long idBitacora = insertaBitacoras(datosPago, total,tipoCambio, comision);
						logger.info("Total MXD / tipoCambio = Total USD: {} / {} = {},",new Object[]{subtotal,tipoCambio,total});
						procom = comercioFin(String.valueOf(datosPago.getIdUser()), 
								String.valueOf(idBitacora), 
								Double.toString(total), 
								"0",
								datosPago.getEmail(),
								comisiones.getAfiliaciones().get("d"),
								"@DuttyFreeVarCurDolares",
								"@DuttyFreeUrlCurDolares");												
					}else{
						//error no se puede realizar el pago
						logger.error("No hay configurado un tipo de cambio NO DISPONIBLE.");
						procom = new ProcomVO(3, "No hay configurado un tipo de cambio NO DISPONIBLE.");
					}
				}			
				logger.debug("Comisiones: {}",utilService.objectToJson(comisiones));			
			}
		}
		
		return procom;
	}	
	
	private long insertaBitacoras(DatosPago dp,Double total,Double tipoCambio, Double comision ){		
		TBitacoraVO tb = new TBitacoraVO(
				dp.getIdUser(), "PAGO DUTTY FREE VISA", String.valueOf(dp.getTotal()), dp.getImei(), "PAGO DUTTY FREE VISA",
				AddcelCrypto.encryptTarjeta(dp.getTarjeta()),dp.getTipo(), dp.getSoftware(), dp.getModelo(),dp.getWkey());
		bm.addBitacora(tb);
		TBitacoraProsaVO tbp = new TBitacoraProsaVO(tb.getIdBitacora(), Long.parseLong(dp.getIdUser()), "PAGO DUTTY FREE VISA", dp.getTotal().doubleValue(), 0.00, dp.getCx(), dp.getCy());
		int idBitacoraProsa = bm.addBitacoraProsa(tbp);
		tbp.setIdBitacoraProsa(idBitacoraProsa);
		DuttyFreeDetalleVO dfd = new DuttyFreeDetalleVO(
				tb.getIdBitacora(), dp.getIdUser(),dp.getDescProducto(), total, dp.getMoneda(), 
				tipoCambio, dp.getNacionalidad(),	dp.getPasaporte(), dp.getNombre_completo(), 
				dp.getFecha_nacimiento(), dp.getNumero_vuelo(), dp.getEmail(), comision);
		bm.addDfDetalle(dfd);
		return tb.getIdBitacora();
	}
	
	private void updateBitacoras(DatosPago dp){
		TBitacoraVO tb = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		tb.setIdBitacora(dp.getIdBitacora());
		tb.setBitStatus(dp.getStatus());
		tb.setBitConcepto(dp.getMsg());
		tb.setBitNoAutorizacion(dp.getNoAutorizacion());
		bm.updateBitacora(tb);
		tbp.setIdBitacora(dp.getIdBitacora());
		tbp.setAutorizacion(dp.getNoAutorizacion());
		logger.info(tbp.toString());
		if(tbp.getAutorizacion()!=null)
			bm.updateBitacoraProsa(tbp);
		// idStatus
		bm.updateDfDetalle(dp.getIdBitacora(), dp.getStatus());
	}
	
	private Double calculaTotal(ComisionVO comisiones,Double subTotal){
		Double comision,total;		
		if(subTotal > comisiones.getMinComPorcentaje() && subTotal< comisiones.getMinCom() ){
			comision=subTotal*comisiones.getComisionPorcentaje();
			total=subTotal+comision;
			logger.info("******* Calculo de pago DuttyFree *******");
			logger.info("Pago por comision %");
			logger.info("Subtotal: {}",subTotal);
			logger.info("% Comision: {}",comisiones.getComisionPorcentaje());
			logger.info("Comision : {}",comision);
			logger.info("Total: {}",total);
			logger.info("+++++++++++++++++++++++++++++++++++++++++");						
		}else{//Se aplica comision fija										
			total=subTotal+comisiones.getComision();
			logger.info("******* Calculo de pago DuttyFree *******");
			logger.info("Pago por comision fija");
			logger.info("Subtotal: {}",subTotal);
			logger.info("Comision Fija: {}",comisiones.getComision());					
			logger.info("Total: {}",total);
			logger.info("+++++++++++++++++++++++++++++++++++++++++");			
		}
		return total;
	}
	
	private ProcomVO comercioFin(String user,String referencia,String monto,String idTramite,String email, String merchant, String currencyParam, String currencyURLParam) {    	        
        String varTotal=formatoMontoProsa(monto);        
        String varMerchant = merchant;
        String varOrderId = referencia;
    	String varStore =obtenParametro("@DuttyFreeVarStore");
    	String varTerm = obtenParametro("@DuttyFreeVarTerm");
    	String varCurrency = obtenParametro(currencyParam);
    	String varAddress = obtenParametro("@DuttyFreeVarAddress");
    	String urlMoneda = obtenParametro(currencyURLParam);
    	String urlBack = obtenParametro("@DuttyFreeUrlBack");

    	logger.info("varMerchant: " + varMerchant +
    				"\nvarOrderid: " + varOrderId +
    				"\nvarStore: " + varStore + 
    				"\nvarTerm: " + varTerm +
    				"\nvarCurrency: " + varCurrency +
    				"\nvarAddress: " + varAddress);
    	

        
        //cambiar por datos correctos
    	String digest = digestEnvioProsa(varMerchant, varStore, varTerm, varTotal, varCurrency, referencia);   
        logger.info("Digest DuttyFree: {}",digest);
       
    	ProcomVO procomObj=new ProcomVO(varTotal, varCurrency, varAddress, referencia, varMerchant, varStore, varTerm, digest, urlBack, user, idTramite,email,urlMoneda);
    	
    	logger.info("User DuttyFree: {}",user);
    	logger.info("Referencia DuttyFree: {}",referencia);
    	logger.info("IdTramite DuttyFree: {}",idTramite);
    	
    	return procomObj;
    }
	
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+centavos;
        }else{
            varTotal=monto.concat("00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
	private String digest(String text){
		String digest="";
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			byte[] sha1=md.digest();
			digest = convertToHex(sha1);
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		return digest;
	}
	
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		return digest(varMerchant+varStore+varTerm+varTotal+varCurrency+varOrderId);		
	}
	
	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {
		StringBuffer digestCad = new StringBuffer()
				.append(transactionProcomV.getEmTotal())
				.append(transactionProcomV.getEmOrderID())
				.append(transactionProcomV.getEmMerchant())
				.append(transactionProcomV.getEmStore())
				.append(transactionProcomV.getEmTerm())
				.append(transactionProcomV.getEmRefNum())
				.append("-")
				.append(transactionProcomV.getEmAuth());
				
		return digest(digestCad.toString());		
	}			
	
	public ResponseProcesaProsa procesaRespuestaProsa(TransactionProcomVO tp,String email, String last4d, String nombrecard) {
		// insertar bitacora ecommerce
		// Valida digest,
		logger.info("last4d:" + last4d + ", nombrecard: " + nombrecard);
		List<DetalleVO> detalleVO = null;
		String paginaRespuesta = "error";
		ResponseProcesaProsa respuesta = new ResponseProcesaProsa();
		String digest = digestRegresoProsa(tp);
		TBitacoraVO b = new TBitacoraVO();
		TBitacoraProsaVO tbp = new TBitacoraProsaVO();
		
		/*
		String patron = "000000";
    	String patron2 = "000000000000";
    	DecimalFormat formato = new DecimalFormat(patron);
    	DecimalFormat formato2 = new DecimalFormat(patron2);
    	
    	Random  rnd = new Random();
    	
    	int aut = (int)(rnd.nextDouble() * 900000);
    	long ref = (long)(rnd.nextDouble() * 900000000);
    	
    	tp.setEmAuth(formato.format(aut));
    	tp.setEmRefNum(formato2.format(ref));
    	*/
    	logger.info("EmAuth:" + tp.getEmAuth() + ", EmRefNum: " + tp.getEmRefNum());
    	
		if (tp.getEmDigest() != null && tp.getEmDigest().equals(digest)) {
			if (!tp.getEmAuth().equals("000000")) {							
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(1);
				b.setBitConcepto("PAGO DUTTY FREE 3DSECURE EXITOSO");
				b.setBitNoAutorizacion(tp.getEmAuth());
				b.setTarjetaCompra("XXXX-XXXX-XXXX-"+last4d);
				bm.updateBitacora(b);
				tbp.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				tbp.setAutorizacion(tp.getEmAuth());
				bm.updateBitacoraProsa(tbp);
				// idStatus
				bm.updateDfDetalle(Integer.parseInt(tp.getEmOrderID()), 1);
				// envio correo
				
				paginaRespuesta = "comerciocon";
				logger.info("Pago correcto");
				
				detalleVO = mapperDF.getDetalle(null, null, null, null, null, null, tp.getEmOrderID());
				if(detalleVO != null && detalleVO.size() >0){
					if(email!=null)
						detalleVO.get(0).setEmail(email);					
					AddCelGenericMail.sendMail(
							utilService.objectToJson(AddCelGenericMail.generatedMail(detalleVO.get(0))));
				}
			} else {					
				b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
				b.setBitStatus(0);
				b.setBitConcepto("PAGO DUTTY FREE 3DSECURE NO AUTORIZADA");
				b.setBitNoAutorizacion(tp.getEmAuth());
				bm.updateBitacora(b);
				// idStatus
				bm.updateDfDetalle(Integer.parseInt(tp.getEmOrderID()), 2);
				paginaRespuesta = "error";
				logger.error("Transacción no autorizada: {}", tp.getEmAuth());
			}
		} else {
			b.setIdBitacora(Long.parseLong(tp.getEmOrderID()));
			b.setBitStatus(2);
			b.setBitConcepto("PAGO DUTTY FREE 3DSECURE ERROR DIGEST");
			b.setBitNoAutorizacion(tp.getEmAuth());
			bm.updateBitacora(b);
			// idStatus
			bm.updateDfDetalle(Integer.parseInt(tp.getEmOrderID()), 2);
			logger.error("Digest incorrecto: INFORMACION INVALIDA ");
			paginaRespuesta = "error";
		}
			respuesta.setRespuesta(paginaRespuesta);
			return respuesta;
	}	
	private Double calculaComision(Double total){
		// Obtener parametros
		Double comision = null;
		try{
			refreshParams();			
			Double comisionFija = (Double)lookupReference("@RIVERACOMISION");
			if(comisionFija == null){
				comisionFija = new Double(mapperDF.getParametro("@RIVERACOMISION"));
				insertReference("@RIVERACOMISION", comisionFija);
			}			
			Double comisionPorcentaje = (Double) lookupReference("@RIVERACOMISIONPOR");
			if(comisionPorcentaje == null){
				comisionPorcentaje = new Double(mapperDF.getParametro("@RIVERACOMISIONPOR"));
				insertReference("@RIVERACOMISIONPOR", comisionPorcentaje);
			}
			Double minCom = (Double)lookupReference("@RIVERAMINCOM");
			if(minCom == null){
				minCom = new Double(mapperDF.getParametro("@RIVERAMINCOM"));
				insertReference("@RIVERAMINCOM", minCom);
			}
			
			Double minComPorcentaje = (Double)lookupReference("@RIVERAMINCOMPOR");
			if(minComPorcentaje == null){
				minComPorcentaje = new Double(mapperDF.getParametro("@RIVERAMINCOMPOR"));
				insertReference("@RIVERAMINCOMPOR", minComPorcentaje);
			}
			logger.info("Parametos: \nTotal = " + total + "\ncomisionFija = " + comisionFija + "\ncomisionPorcentaje = "+ comisionPorcentaje 
						+ "\nminCom = " + minCom + "\nminComPorcentaje = "+ minComPorcentaje);	
			if(total.doubleValue() >= minComPorcentaje.doubleValue() && total.doubleValue() <= minCom.doubleValue() )
				comision=total*comisionPorcentaje;
			else										
				comision = comisionFija;		
			logger.info("Comision calculada: " + comision);
		} catch(Exception e){
			logger.error("Error al calcular comision: " + e.getMessage());
		}
		return comision;
	}
	private Object lookupReference(String jndi)
    {
		logger.info("inicio lookupReference:");
        InitialContext contexto;
        Object obj;
        try {
        	logger.info("Buscando en el contexto :"+jndi);
            contexto = new InitialContext();
            obj = contexto.lookup(jndi);
            logger.info(jndi+" Encontrado, del tipo "+obj.getClass().getName());
        } catch (NamingException e) {
        	logger.info(jndi+ " no esta en el contexto");                       
            obj = null;
        }       
        logger.info("Fin lookupReference:");
        return obj;
    }
    private void removeReference(String jndi) 
    {
    	logger.info("inicio removeReference:");
        InitialContext contexto;
        try {
            contexto = new InitialContext();
            contexto.unbind(jndi);
            contexto.removeFromEnvironment(jndi);
        } catch (NamingException e) {
        	logger.info("Fin removeReference, error: " + e.getMessage());   
        }
        logger.info("Fin removeReference:");       
    }
 
    private void insertReference(String jndi, Object obj) throws Exception
    {
    	logger.info("inicio insertReference:");
        InitialContext contexto;
        contexto = new InitialContext();
        if(lookupReference(jndi)==null){
        	logger.info("insertando en el contexto: "+ jndi);
            contexto.addToEnvironment(jndi,obj);
            contexto.bind(jndi,obj);
        } else
        {
        	logger.info("actualizando en el contexto : "+ jndi);
            contexto.rebind(jndi,obj);
        }
        logger.info("fin insertReference:");
    }
    private void refreshParams(){
    	
    	try{
    		SimpleDateFormat formato = new SimpleDateFormat("ddMMyyyy");
    		Calendar cal =  GregorianCalendar.getInstance();
    		Date fecha = cal.getTime();
    		String hoy = formato.format(fecha);
    		String fechaVigencia = (String)lookupReference("fechaVigencia");
    		logger.info("Hoy:" + hoy);
    		logger.info("Fecha vigencia parametros contexto:" + fechaVigencia);
    		if(fechaVigencia!=null){
    			if(!hoy.equals(fechaVigencia)){
    				removeReference("@RIVERACOMISION");
    				removeReference("@RIVERACOMISIONPOR");
    				removeReference("@RIVERAMINCOM");
    				removeReference("@RIVERAMINCOMPOR");
    				removeReference("fechaVigencia");
    				insertReference("fechaVigencia", hoy);
    			}
    		}else
    			insertReference("fechaVigencia", hoy);
    	}catch(Exception e){
    		logger.error("Error al refrescar los parametros: "+ e.getMessage());
    	}
    }
	private String obtenParametro(String reference){
		String param = null;
		param = mapper.getParametro(reference);
		return param;
	}

}
