package com.addcel.duttyfree.controller;

import java.io.IOException;
import java.util.Locale;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.duttyfree.model.vo.RespuestaAmexVO;
import com.addcel.duttyfree.model.vo.pagos.ProcomVO;
import com.addcel.duttyfree.model.vo.pagos.ProsaPagoVO;
import com.addcel.duttyfree.model.vo.pagos.TransactionProcomVO;
import com.addcel.duttyfree.services.DuttyFreePagoAMEX;
import com.addcel.duttyfree.services.DuttyFreePagosService;
import com.addcel.duttyfree.services.DuttyFreeService;
import com.addcel.duttyfree.model.vo.response.ResponseProcesaProsa;

/**
 * Handles requests for the application home page.
 */
@Controller
public class DuttyFreeController {
	
	private static final Logger logger = LoggerFactory.getLogger(DuttyFreeController.class);
	@Autowired
	private DuttyFreeService dfService;
	@Autowired
	private DuttyFreePagosService dfpService;
	@Autowired
	private DuttyFreePagoAMEX amexService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);				
		return "home";
	}
	
	@RequestMapping(value = "/addUser",method=RequestMethod.POST)
	public @ResponseBody String addUser(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: /addUser");
		return dfService.addUser(json);
	}
	
	@RequestMapping(value = "/login",method=RequestMethod.POST)
	public @ResponseBody String login(@RequestParam("json")String json) {	
		logger.info("Dentro del servicio: /login");
		return dfService.login(json);
	}
	
	@RequestMapping(value = "/updateUser",method=RequestMethod.POST)
	public @ResponseBody String updateUser(@RequestParam("json")String json) {
		logger.info("Dentro del servicio: /updateUser");
		return dfService.updateUser(json);
	}
	
	@RequestMapping(value = "/getTiendas",method=RequestMethod.POST)
	public @ResponseBody String listaTiendas(@RequestParam("json") String data) {	
		logger.info("Dentro del servicio: /getTiendas");
		return dfService.listaTiendas(data);
	}
	
	@RequestMapping(value = "/findUser",method=RequestMethod.POST)
	public @ResponseBody String findUser(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /findUser");
		return dfService.findUser(data);
	}
	
	@RequestMapping(value = "/getRoles",method=RequestMethod.POST)
	public @ResponseBody String listaRoles(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /getRoles");
		return dfService.listaRoles(data);
	}
	
	@RequestMapping(value = "/getVendedor",method=RequestMethod.POST)
	public @ResponseBody String getVendor(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /getVendedor");
		return dfService.getVendor(data);
	}
	
	@RequestMapping(value = "/getToken")
	public @ResponseBody String getToken() {		
		logger.info("Dentro del servicio: /getToken");
		return dfService.generaToken();
	
	}
			
	@RequestMapping(value = "/deleteUser",method=RequestMethod.POST)
	public @ResponseBody String deleteUser(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /deleteUser");
		return dfService.deleteUser(jsonEnc);	
	}
	
	/**
	 * Se utiliza para los roles supervisor y vendedor
	 * @param jsonEnc
	 * @return string encriptado con la respuesta del servicio
	 */
	@RequestMapping(value = "/updatePass",method=RequestMethod.POST)
	public @ResponseBody String actualizarPassword(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /updatePass");
		return dfService.actualizaPassword(jsonEnc,0);
	}
	
	@RequestMapping(value = "/updatePassStatus",method=RequestMethod.POST)
	public @ResponseBody String actualizarPasswordStatus(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /updatePassStatus");
		return dfService.actualizaPassword(jsonEnc,1);
	}
	
	@RequestMapping(value = "/pago-prosa",method=RequestMethod.POST)
	public ModelAndView datosPago(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-prosa");
		ModelAndView mav=new ModelAndView("error");
		ProcomVO procom = dfpService.procesaPago(jsonEnc);
		if(procom!=null){
			if(procom.getIdError()==0)
				mav=new ModelAndView("comerciofin");
			mav.addObject("prosa", procom);
			mav.addObject("processId", "1");
		}
		return mav;	
	}
	
	@RequestMapping(value = "/pago-visa")
	public @ResponseBody String datosPagoVisa(@RequestParam("json") String jsonEnc) {	
		logger.info("Dentro del servicio: /pago-visa");

		return dfpService.procesaPagoVisa(jsonEnc);	
	}
	
	@RequestMapping(value = "/pago-amex") //*******************************************************
	public @ResponseBody String pagoAmex(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: //pago-amex");
		return amexService.procesaPago(data);
	}
	
	@RequestMapping(value = "/comercio-fin")
	public String comercioFin(Model model) {
		logger.info("Dentro del servicio: /comercio-fin");
		//dfpService.comercioFin(user, referencia, monto, idTramite)
		//model.addAttribute("prosa", arg1);
		return "comerciofin";	
	}
	
	@RequestMapping(value = "/pagina-prosa")
	public String paginaTestProsa(Model model) {
		logger.info("Dentro del servicio: /pagina-prosa");
		//dfpService.comercioFin(user, referencia, monto, idTramite)
		//model.addAttribute("prosa", arg1);
		return "pagina-prosa";	
	}
	
	@RequestMapping(value = "/comercio-con",method=RequestMethod.POST)
	public ModelAndView respuestaProsa(HttpServletRequest request) {

		String EM_Response = null, EM_Total = null, EM_OrderID = null, 
				   EM_Merchant = null, EM_Store = null, EM_Term = null,
				   EM_RefNum = null, EM_Auth = null, EM_Digest = null, 
				   email = null, cc_numberback = null, cc_nameback = null;

		logger.info("Dentro del servicio: /comercio-con");
		ModelAndView mav=new ModelAndView("error");
		ResponseProcesaProsa respuesta = null;
						
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        logger.info(key);
	        String[] vals = parameters.get(key);
	        if(key.equals("EM_Response"))
	        	EM_Response = vals[0];
	        else if(key.equals("EM_Total"))
	        	EM_Total = vals[0];
	        else if(key.equals("EM_OrderID"))
	        	EM_OrderID = vals[0];
	        else if(key.equals("EM_Merchant"))
	        	EM_Merchant = vals[0];
	        else if(key.equals("EM_Store"))
	        	EM_Store = vals[0];
	        else if(key.equals("EM_Term"))
	        	EM_Term = vals[0];
	        else if(key.equals("EM_RefNum"))
	        	EM_RefNum = vals[0];
	        else if(key.equals("EM_Auth"))
	        	EM_Auth = vals[0];
	        else if(key.equals("EM_Digest"))
	        	EM_Digest = vals[0];
	        else if(key.equals("email"))
	        	email = vals[0];
	        else if(key.equals("cc_number")){
	        	cc_numberback = vals[0];
	        }
	        else if(key.equals("cc_name"))
	        	cc_nameback = vals[0];

	        
	        for(String val : vals)
	        	logger.info(" -> " + val);
	    }
		
		TransactionProcomVO tp= new TransactionProcomVO();
		tp.setEmAuth(EM_Auth);
		tp.setEmMerchant(EM_Merchant);
		tp.setEmOrderID(EM_OrderID);
		tp.setEmRefNum(EM_RefNum);
		tp.setEmResponse(EM_Response);
		tp.setEmStore(EM_Store);
		tp.setEmTerm(EM_Term);
		tp.setEmTotal(EM_Total);
		tp.setEmDigest(EM_Digest);
		logger.debug("email: {}",email);		
		
		respuesta = dfpService.procesaRespuestaProsa(tp,email,cc_numberback,cc_nameback);
		if(respuesta != null ){
			mav = new ModelAndView(respuesta.getRespuesta());
			mav.addObject("prosa", tp);
			mav.addObject("prosa", tp);
			mav.addObject("processId", "2");
		}
		return mav;	
	}
	
	@RequestMapping(value = "/busquedaPagos",method=RequestMethod.POST) 
	public @ResponseBody String busquedaPagos(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /busquedaPagos");
		return dfService.busquedaPagos(data);
	}
	
	@RequestMapping(value = "/reenvioRec",method=RequestMethod.POST) 
	public @ResponseBody String reenvioRec(@RequestParam("json") String data) {
		logger.info("Dentro del servicio: /reenvioRec");
		return dfService.reenvioRecibo(data);
	}
	@RequestMapping(value = "/pago-amex-prov",method=RequestMethod.POST)
	public ModelAndView pagoAmexProv(HttpServletRequest request) {

		String Tarjeta = null;
		String Vigencia = null;
		String Monto = null;
		String Cvv = null;
		String Direccion = null;
		String Cp = null; 
		logger.info("Dentro del servicio: /pago-amex-prov");
		ModelAndView mav=new ModelAndView("AmexResp");
		RespuestaAmexVO respuesta = null;
						
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        logger.info(key);
	        String[] vals = parameters.get(key);
	        if(key.equals("Tarjeta"))
	        	Tarjeta = vals[0];
	        else if(key.equals("Vigencia"))
	        	Vigencia = vals[0];
	        else if(key.equals("Monto"))
	        	Monto = vals[0];
	        else if(key.equals("cvv"))
	        	Cvv = vals[0];
	        else if(key.equals("Direccion"))
	        	Direccion = vals[0];
	        else if(key.equals("cp"))
	        	Cp = vals[0];

	        for(String val : vals)
	        	logger.info(" -> " + val);
	    }
		
		try {
			respuesta = amexService.procesaPagoProv(Tarjeta, Vigencia, Monto, Cvv, Direccion, Cp);
		} catch (IOException e) {
			respuesta = new RespuestaAmexVO();
			respuesta.setError("500");
			respuesta.setErrorDsc(e.getMessage());
		}
		if(respuesta != null ){
			mav.addObject("ramex", respuesta);
		}
		return mav;	
	}
	@RequestMapping(value = "/pago-visa-prov",method=RequestMethod.POST)
	public ModelAndView pagoVisaProv(HttpServletRequest request) {

		String Tarjeta = null;
		String Vigencia = null;
		String Monto = null;
		String Cvv = null;
		String Nombre = null;

		logger.info("Dentro del servicio: /pago-visa-prov");
		ModelAndView mav=new ModelAndView("VisaResp");
		ProsaPagoVO respuesta = null;
						
		@SuppressWarnings("unchecked")
		Map<String, String[]> parameters = request.getParameterMap();

	    for(String key : parameters.keySet()) {
	        logger.info(key);
	        String[] vals = parameters.get(key);
	        if(key.equals("Tarjeta"))
	        	Tarjeta = vals[0];
	        else if(key.equals("Vigencia"))
	        	Vigencia = vals[0];
	        else if(key.equals("Monto"))
	        	Monto = vals[0];
	        else if(key.equals("cvv"))
	        	Cvv = vals[0];
	        else if(key.equals("Nombre"))
	        	Nombre = vals[0];

	        for(String val : vals)
	        	logger.info(" -> " + val);
	    }
		
		try {
			respuesta = dfpService.pagoVisaProb(Tarjeta, Vigencia, Nombre, Cvv, Monto);
		} catch (Exception e) {
			respuesta = new ProsaPagoVO();
			respuesta.setError(500);
			respuesta.setMsg(e.getMessage());
		}
		if(respuesta != null ){
			mav.addObject("rvisa", respuesta);
		}
		return mav;	
	}
}