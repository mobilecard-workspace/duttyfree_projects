<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Respuesta Pago Provisional</title>
</head>
<body>

	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2">Pago procesado, resultado.</td>
		</tr>
		<tr>
			<td></br></td>
			<td></td>
		</tr>
		<tr>
			<td>Transaccion:</td>
			<td>${ramex.transaction}</td>
		</tr>
		<tr>
			<td>Code:</td>
			<td>${ramex.code}</td>
		</tr>
		<tr>
			<td>Desc:</td>
			<td>${ramex.dsc}</td>
		</tr>
		<tr>
			<td>Err:</td>
			<td>${ramex.error}</td>
		</tr>
		<tr>
			<td>Desc Err:</td>
			<td>${ramex.errorDsc}</td>
		</tr>				
	</table>
</body>
</html>