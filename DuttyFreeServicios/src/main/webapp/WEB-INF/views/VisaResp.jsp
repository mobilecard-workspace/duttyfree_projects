<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Respuesta Pago Provisional Visa</title>
</head>
<body>

	<table cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="2">Pago procesado, resultado.</td>
		</tr>
		<tr>
			<td></br></td>
			<td></td>
		</tr>
		<tr>
			<td>Err:</td>
			<td>${rvisa.error}</td>
		</tr>
		<tr>
			<td>Msg:</td>
			<td>${rvisa.msg}</td>
		</tr>
		<tr>
			<td>Transaccion:</td>
			<td>${rvisa.transaccion}</td>
		</tr>
		<tr>
			<td>Autorizacion:</td>
			<td>${rvisa.autorizacion}</td>
		</tr>		
		<tr>
			<td>Transaccion Id:</td>
			<td>${rvisa.transactionId}</td>
		</tr>
		<tr>
			<td>Banco Adquirente:</td>
			<td>${rvisa.bancoAdquirente}</td>
		</tr>
		<tr>
			<td>Descripcion Rechazo:</td>
			<td>${rvisa.descripcionRechazo}</td>
		</tr>		
	</table>
</body>
</html>